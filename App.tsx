/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './screen/HomeScreen';
import LoginSignUpScreen from './screen/auth/LoginSignUpScreen';
import FetchDataScreen from './screen/fetch/FetchDataScreen';
import PostDetailsScreen from './screen/fetch/PostDetailsScreen';
import { Provider } from 'react-redux';
import store from './store';



export type RootStackParamList = {
  Home: undefined,
  Login: undefined,
  Fetch: undefined,
  PostDetails: {
    item: {}
  }
}


const Stack = createNativeStackNavigator<RootStackParamList>();



function App(): JSX.Element {

  return (
    <Provider store={store}>
      <NavigationContainer >
        <Stack.Navigator initialRouteName="Home" >
          <Stack.Screen name="Home" options={
            {
              title: 'Welcome,Guest',

            }
          } component={HomeScreen} />
          <Stack.Screen name="Login" component={LoginSignUpScreen} />
          <Stack.Screen name="Fetch" component={FetchDataScreen} />
          <Stack.Screen name="PostDetails" component={PostDetailsScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}


export default App;

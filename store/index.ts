import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import fetchSaga from "./fetchSaga";
import fetchReducer from "./fetchSlice";

const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
    reducer: fetchReducer,
    middleware: [sagaMiddleware]
})

sagaMiddleware.run(fetchSaga);

export default store;
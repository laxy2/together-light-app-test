import { call, put, takeEvery } from "redux-saga/effects";
import { fetchData, fetchDataFail, onEndreached } from "./fetchSlice";

import { BASE_URL } from "../utils/type";
type actionPayload = {
    payload: {
        page: number,
        page_size: number,
        list: []
    }
}


function* letsFetch(action: actionPayload): any {
    console.log("lets fetch", action);
    const { page, page_size, list } = action.payload;
    console.log("list lenth: ", list);


    try {
        let data = yield call(() => fetch(BASE_URL + `?_page=${page}&pageSize=${page_size}`));
        const dataJson = yield data.json();
        console.log("dataJson: ", dataJson);

        if (dataJson?.length > 0) {

            yield put(fetchData([...list, ...dataJson]));
        } else {
            yield put(onEndreached(true))
        }

    } catch (error: any) {
        console.log("error: ", error);

        yield put(fetchDataFail(error.message))
    }
}


function* fetchSaga() {
    console.log("- - - - - - - - - -" + "\n");
    console.log("here");

    yield takeEvery("fetch/fetchDataStart", letsFetch)
}

export default fetchSaga;
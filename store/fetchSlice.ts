import { createSlice } from "@reduxjs/toolkit";
import { initialStateProps } from "../utils/type";

let initialState: initialStateProps = {

    list: [],
    error: null,
    loading: false,
    page: 1,
    page_size: 10,
    endReached: false
}

export const fetchSlice = createSlice({
    name: "fetch",
    initialState,
    reducers: {
        setPageAndPageSize: (state, action) => {
            if (action.payload) {
                state.page = action.payload.page || state.page;
                state.page_size = action.payload.page_size || state.page_size;
            }
        },
        fetchDataStart: (state, action) => {

            state.loading = true;
            if (action.payload) {
                state.page = action.payload.page || state.page;
                state.page_size = action.payload.page_size || state.page_size;
            }

        },
        fetchData: (state, action) => {

            state.list = action.payload;
            state.loading = false
        },
        fetchDataFail: (state, action) => {



            state.error = action.payload


        },
        onEndreached: (state, action) => {
            console.log(action);

            state.endReached = true
        }
    }
})

export const { onEndreached, setPageAndPageSize, fetchDataStart, fetchData, fetchDataFail } = fetchSlice.actions;

export default fetchSlice.reducer;
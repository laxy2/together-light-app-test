/**
 * @format
 */

import 'react-native';

import fetchMock from 'jest-fetch-mock'; // Import jest-fetch-mock

import { it } from '@jest/globals';
import { fireEvent, render, waitFor } from '@testing-library/react-native';
import FetchDataScreen from '../screen/fetch/FetchDataScreen';
import { fetchData, fetchDataStart } from '../store/fetchSlice';
// import { useDispatch } from 'react-redux';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { BASE_URL } from '../utils/type';
import { runSaga } from 'redux-saga';
import fetchSaga from '../store/fetchSaga';


describe('FetchDataScreen', () => {
  const initialState = {
    list: [],
    error: null,
    loading: false,
    page: 1,
    page_size: 10,
    endReached: false
  };
  const mockStore = configureStore();
  let store;
  it('renders the component correctly', () => {
    store = mockStore(initialState);

    const { getByText } = render(<Provider store={store}><FetchDataScreen /></Provider>);

    expect(getByText('DATA NOT FOUND')).toBeTruthy();
  });


  it('should call api and dispatch success action', async () => {
    const dummyPosts = [
      {
        userId: 1,
        id: 1,
        title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body: "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto"
      }
    ];


    const fetchMock = jest.spyOn(global, 'fetch').mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(dummyPosts),
      })
    );

    const dispatched: any[] = [];
    const result = await runSaga(
      {
        dispatch: (action) => dispatched.push(action),
      },
      fetchSaga
    );


    expect(fetchMock).toHaveBeenCalledTimes(1);


    expect(dispatched).toEqual([fetchData(dummyPosts)]);


    fetchMock.mockRestore();
  });

});


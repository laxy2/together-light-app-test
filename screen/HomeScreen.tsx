import React, { PropsWithChildren, useEffect } from 'react'

import { Button, View, StyleSheet } from 'react-native'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { RootStackParamList } from '../App'
import { useDispatch, useSelector } from 'react-redux'
import { fetchDataStart } from "../store/fetchSlice";

type HomeProps = NativeStackScreenProps<RootStackParamList, "Home">


const HomeScreen = ({ navigation }: HomeProps): JSX.Element => {

    // const fetchDataState = useSelector(state => state.list)
    const dispatch = useDispatch()
    // console.log(fetchDataState);

    useEffect(() => {
        dispatch(fetchDataStart({ page: 1, page_size: 10, list: [] }))

    }, []);



    function ClickHandler(type: number) {

        if (type == 0) {
            navigation.navigate("Login")
        } else {
            navigation.navigate("Fetch")
        }

    }



    return (
        <View style={style.container}>
            <Button

                title="Login/SignUp Task" onPress={() => ClickHandler(0)} />
            <Button title="Fetch Data Task" onPress={() => ClickHandler(1)} />
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        padding: 24,
        gap: 30,
        justifyContent: "center"
    },
    btn: {
        borderRadius: 44
    }
})

export default HomeScreen

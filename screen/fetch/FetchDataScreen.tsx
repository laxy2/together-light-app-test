import React, { memo, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { FlatList, StyleSheet, Text, View, ActivityIndicator, RefreshControl, TouchableWithoutFeedback } from 'react-native'
import { RootStackParamList } from '../../App'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { useDispatch, useSelector } from 'react-redux'
import { initialStateProps } from '../../utils/type'
import { fetchData, fetchDataStart, setPageAndPageSize } from '../../store/fetchSlice'
type FetchDataProps = NativeStackScreenProps<RootStackParamList, "Fetch">

const FetchDataScreen = ({ navigation }: FetchDataProps): JSX.Element => {


    // const [list, setList] = useState<any[]>([]);
    const list = useSelector((state: initialStateProps) => state.list);
    const page = useSelector((state: initialStateProps) => state.page);
    const pageSize = useSelector((state: initialStateProps) => state.page_size);
    const isLoading = useSelector((state: initialStateProps) => state.loading)
    const endReached = useSelector((state: initialStateProps) => state.endReached)

    // const [page, setPage] = useState(1);
    // const [isLoading, setIsLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);
    const dispatch = useDispatch()

    // useEffect(() => {

    //     console.log(page);
    //     console.log(pageSize);

    //     // fetchData()
    //     // dispatch(fetchDataStart({ page: page, page_size: 10 }))


    // }, [page]);

    const handleRefresh = () => {
        setRefreshing(true);
        // setList([]);
        // setPage(1);
        // fetchData();
        dispatch(fetchDataStart({ page: 1, page_size: 10, list: [] }))
        setRefreshing(false)

    };

    async function fetchDataLocal() {
        if (!isLoading && !endReached) {
            // dispatch(fetchData(list))
            dispatch(fetchDataStart({ page: page + 1, page_size: pageSize, list: list }))
            // setPage(page + 1)
        }
        // setIsLoading(true)
        // try {
        //     let postData = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&pageSize=${PAGE_SIZE}`);

        //     let postDataArray = await postData.json()
        //     console.log(postDataArray.length);
        //     if (postDataArray?.length > 0) {
        //         setList([...list, ...postDataArray])

        //     }

        // } catch (error) {
        //     console.log(error);
        // }
        // finally {
        //     setIsLoading(false);
        //     setRefreshing(false);
        // }


    }
    function EmptyView() {
        return <Text style={style.emptyView}>DATA NOT FOUND</Text>
    }
    const redirectToPostDetails = (item: any) => {

        navigation.navigate("PostDetails", {
            item: {
                ...item
            }
        })

    }

    const Item = memo(({ item, redirectToPostDetails }: { item: any; redirectToPostDetails: Function }) => (
        <TouchableWithoutFeedback onPress={() => redirectToPostDetails(item)}>
            <View style={[style.card, style.shadowProp]}>
                <Text style={style.title}>Title:</Text>
                <Text>{item.title + " ID: " + item.id + ""}</Text>
                <Text style={style.title}>Body:</Text>
                <Text>{item.body}</Text>
            </View>
        </TouchableWithoutFeedback>
    ));

    return (
        <View>


            <FlatList
                data={list}
                ListEmptyComponent={EmptyView}
                renderItem={({ item }) => <Item item={item} redirectToPostDetails={redirectToPostDetails} />}

                keyExtractor={item => item.id?.toString()}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={handleRefresh} />
                }
                onEndReached={() =>
                    // dispatch()
                    // dispatch(setPageAndPageSize({ page: page + 1, page_size: 10 }))
                    fetchDataLocal()
                    // setPage(page + 1)
                }
                onEndReachedThreshold={0.1}

                ListFooterComponent={() => endReached ? (
                    <Text>No more items to load</Text>
                ) : isLoading && <ActivityIndicator />}
            />
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,

    },

    card: {
        backgroundColor: 'white',
        borderRadius: 8,
        paddingVertical: 25,
        paddingHorizontal: 25,
        width: '100%',
        marginVertical: 10,
    },
    shadowProp: {
        shadowOffset: { width: -2, height: 4 },
        shadowColor: '#171717',
        shadowOpacity: 0.2,
        shadowRadius: 3,
    },
    title: {
        fontWeight: "bold"
    },
    emptyView: {
        fontSize: 18,
        fontWeight: "bold",
        textAlign: "center",
    }
})

export default FetchDataScreen

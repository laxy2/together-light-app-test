import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { RootStackParamList } from '../../App'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

type PostDetailsProps = NativeStackScreenProps<RootStackParamList, "PostDetails">

const PostDetailsScreen = ({ navigation, route }: PostDetailsProps) => {

    const postDetails = route.params.item;


    return (
        <View style={style.container}>
            <View style={style.row}><Text style={style.title}>UserId</Text><Text>{postDetails.userId}</Text></View>
            <View style={style.row}><Text style={style.title}>Title</Text><Text>{postDetails.title}</Text></View>
            <View style={style.row}><Text style={style.title}>Body</Text><Text>{postDetails.body}</Text></View>
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: 10
    },

    row: {
        gap: 10,
        padding: 10,
        flexDirection: "row"
    },

    title: {
        fontWeight: "bold"
    },

    description: {

    }


})
export default PostDetailsScreen;
import React, { useRef, useState } from 'react'

import { Alert, Button, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, View } from "react-native";

function LoginSignUpScreen(): JSX.Element {
    const email = "patellaxya2@gmail.com"
    const psw = "laxy1234"
    const [inputEmail, setInputEmail] = useState(email);
    const [inputPsw, setInputPsw] = useState(psw);
    const [result, setResult] = useState(0);

    function login() {


        console.log(inputEmail);
        console.log(inputPsw);

        if (inputPsw == psw && inputEmail == email) {
            setResult(1)
        } else {
            setResult(-1)
        }


    }


    return (
        <SafeAreaView style={style.container}>
            <ScrollView>
                <TextInput value={inputEmail} onChangeText={text => setInputEmail(text)} placeholder="Email" style={style.textInput} keyboardType="email-address" />
                <TextInput
                    secureTextEntry={true}
                    value={inputPsw}
                    onChangeText={text => setInputPsw(text)}
                    placeholder="Password" style={style.textInput}



                />

                <View style={style.btn}><Button onPress={login} title="Login" /></View>
                {
                    result != 0 && (result == 1 ? <Text style={style.txtSuccess}>Successful Login! </Text>
                        : <Text style={style.txtFail}>Login Failed!</Text>)
                }
            </ScrollView>
        </SafeAreaView>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center"
        ,
    },
    textInput: {


        height: 40,
        margin: 12,
        padding: 10,
        borderWidth: 1,


    },

    btn: {
        height: 40,
        margin: 12,

    },

    txtSuccess: {
        textAlign: "center",
        color: "green"
    },
    txtFail: {
        textAlign: "center",
        color: "red"
    }


})

export default LoginSignUpScreen;
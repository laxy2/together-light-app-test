export interface ItemProps {
    "userId": number,
    "id": number,
    "title": string,
    "body": string
}
export interface fetchItems {
    id: number | string,
    title: string,
    body: string

}
export interface initialStateProps {
    list?: fetchItems[],
    error?: null,
    loading: boolean,
    page: number,
    page_size: number,
    endReached: boolean,
}

export const BASE_URL = "https://jsonplaceholder.typicode.com/posts";